<?php 

	class AFF_User_Relationship {

	    static $table = 'mh_user_relationships';


	    static function initRelationShip(){
            $users = AFF_User::getList([], 1, 2000);
            
            foreach($users['data'] as $user){
                $db = MH_Query::init(null, self::$table);
                $record = $db->where('ancestor_id', $user['ID'])
                    ->where('descendant_id', $user['ID'])
                    ->where('distance', 0)
                    ->first();
                if(!$record)
                    $db->insert([
                        'ancestor_id' => $user['ID'],
                        'descendant_id' => $user['ID'],
                        'distance' => 0,
                    ]);
            }
	    }

        static function getAncestor($user_id, $distance = 0){

            $db = MH_Query::init(null, self::$table);
            $query = $db->select('ancestor_id, distance')
                ->where('descendant_id', $user_id)
                ->order_by('distance', 'ASC');
            if($distance)
                $query = $query->where('distance', '<=', $distance);
            return $query->get();

        }

        static function getDescendants($user_id, $distance, $pluck = ''){
            $db = MH_Query::init(null, self::$table);
            $query = $db->select('descendant_id, distance')
                ->where('ancestor_id', $user_id)
                ->order_by('distance', 'ASC');
            if($distance)
                $query = $query->where('distance', '<=', $distance);
            
            if($pluck)
                return $query->order_by('distance', 'ASC')->pluckOneColumnArray($pluck);

            return $query->order_by('distance', 'ASC')->get();
        }

        
        static function setRelationship($descendant_id, $ancestor_id){
            $level = AFF_Config::getConfig('relationship_level');
            if(!$level)
                return;
            $ancestors = self::getAncestor($ancestor_id, ($level - 1));
            // debug($ancestors);
            if($ancestors){
                foreach($ancestors as $an){
                    $db = MH_Query::init(null, self::$table);

                    $db->where('ancestor_id', $an['ancestor_id']);
                    $db->where('descendant_id', $descendant_id);
                    $db->where('distance', $an['distance'] + 1);

                    $db->findOrInsert([
                        'ancestor_id' => $an['ancestor_id'],
                        'descendant_id' => $descendant_id,
                        'distance' => $an['distance'] + 1,
                    ]);
                }
            }            
            
        }

	    


	}
    // debug(AFF_User_Relationship::setRelationship(4,2));
    // debug(AFF_User_Relationship::initRelationShip());

?>
