<?php 

	class AFF_User_Order extends AFF_App{

	    static $table = 'mh_user_order';
        static $total = [
            'amount_directly' => 0,
            'amount_website_wallet' => 0,
            'amount_affiliate_percent' => 0,
            'amount_affiliate_back_percent' => 0,
            'amount_lucky_wallet' => 0,
            'amount_one_percent_wallet' => 0,
        ];

        static function approveCommission($order_id, $status){
            $settings = self::getSettings();

            $rows = MH_Query::init(null, self::$table)->where('order_id', $order_id)->where('status', 0)->get();
            if(!$rows)
                return;

            foreach ($rows as $key => $row) {
                $user = AFF_User::getUserBy(['column' => 'ID', 'value' => $row['user_id']]);
                if(!$user)
                    return;

                //SETTING INCLUDE COMMISSION OF CHILDREN TO PARENT
                if($row['level'] == 0){
                    
                    //Increase Level when user reach income
                    $new_income = $user['income'] + $row['total'];
                    $commission_user_levels = $settings['commission_user_levels'];
                    $user_level = $settings['user_level'];

                    $level_index = null;
                    foreach ($commission_user_levels as $key => $level) {
                        if($key+1  > $user_level || $level['income'] == '')
                            break;
                        if($user['income'] >= $level['income'])
                            $level_index = $key;
                    }
                 
                    if($level_index != null && $level_index != $user['level'])
                    {
                        $data = [
                           'level' => $level_index
                        ];
                        MH_Query::init(null, 'users')->where('ID', $user['ID'])->update($data);
                    }   

                    if($settings['affiliate_percent'] +  $settings['affiliate_back_percent'] > 0){

                        $commission_ = ($row['commission'] * 100) / ($settings['affiliate_percent'] +  $settings['affiliate_back_percent']);
                        // + Ví cho Website
                        $settings['website_wallet'] += $commission_/100 * $settings['website_wallet_percent'];
                        AFF_Config::setConfig('website_wallet', $settings['website_wallet']);

                        // + Ví cho quỹ bốc thăm
                        $settings['lucky_wallet'] += $commission_/100 * $settings['affiliate_lucky_percent'];
                        AFF_Config::setConfig('lucky_wallet', $settings['lucky_wallet']);

                        // + Ví cho quỹ 1%
                        $settings['one_percent_wallet'] += $commission_/100 * $settings['affiliate_last_percent'];
                        AFF_Config::setConfig('one_percent_wallet', $settings['one_percent_wallet']);
                    } 

              


                    $result = AFF_User::changeBalance($user['ID'], $row['commission'], 1, $row['total'], $row['description']);
                }
                else if($settings['income_include_child'] == 'true')
                    $result = AFF_User::changeBalance($user['ID'], $row['commission'], 1, $row['total'], $row['description']);
                else
                    $result = AFF_User::changeBalance($user['ID'], $row['commission'], 1, 0, $row['description']);

                

                if($row['level'] == 0 && AFF_Config::getConfig('aff_email') == 'true'){
                    $mail_content = AFF_Config::getConfig('noti_email_order_completed');
                    AFF_SendMail( $user['user_email'], 'Đơn hàng thành công ' . $user['user_login'], $mail_content, [
                        '[user_name]' => $user['user_login'],
                        '[order_id]' => $order_id,
                        '[order_total]' => $row['total'],
                        '[commision]' => number_format($row['commission']),
                    ]);
                }

                if($result){
                    MH_Query::init(null, self::$table)->where('id', $row['id'])->update(['status' => 1]);
                }
            }
            return;
        }

        static function create($user, $order, $ref_path, $ref_product, $ref_coupon){
            $settings = self::getSettings();

            if($settings['aff_mode'] == 'order_mode')
                $data = self::setCommissionOrderMode($user, $order, $ref_coupon);
                 
            if($settings['aff_mode'] == 'product_mode')
                $data = self::setCommissionProductMode($user, $order, $ref_coupon);
            
            // debug($data);
                
            $order_id = $order->get_id();
            $products = [];
            $items = $order->get_items();
            foreach ( $items as $item ) {
				$id = $item->get_product_id();
				$image = get_the_post_thumbnail_url($id, 'thumbnail');
				
				$products[] = [
					'name' => $item->get_name(),
					'product_id' => $id,
					'product_variation_id' => $item->get_variation_id(),
					'quantity' => $item->get_quantity(),
					'total'	   => $item->get_total(),
					'image' => $image ? $image :'https://thumbs.dreamstime.com/b/no-image-available-icon-vector-illustration-flat-design-140476186.jpg',
					'link' => get_permalink($id),
				];
			}

            $order_json = [
				'order_id' 				=> $order_id,
				'total'	   				=> $order->get_total(),
                'customer_name'			=> $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
				'customer_phone'		=> $order->get_billing_phone(),
				'customer_address'		=> $order->get_billing_address_1(),
				'products'				=> $products,
                'shipping_fee'          => intval($order->get_shipping_total()),
				'sub_total'             => WC()->cart->subtotal,
			];
            


            //Hoa hồng
            $commission = $data['commission']/100 * $settings['affiliate_percent'] + $data['commission']/100 * $settings['affiliate_back_percent'];

            self::$total['amount_directly'] += $commission;

            if($settings['affiliate_percent'] +  $settings['affiliate_back_percent'] > 0){

                // + Ví cho Website
                $website_wallet += $data['commission']/100 * $settings['website_wallet_percent'];
                self::$total['amount_website_wallet'] += $website_wallet;
                // + Ví cho quỹ bốc thăm
                $lucky_wallet += $data['commission']/100 * $settings['affiliate_lucky_percent'];
                self::$total['amount_lucky_wallet'] += $lucky_wallet;

                // + Ví cho quỹ 1%
                $one_percent_wallet += $data['commission']/100 * $settings['affiliate_last_percent'];
                self::$total['amount_one_percent_wallet'] += $one_percent_wallet;

            } 

            $the_rest_commission = $data['commission'] - array_sum(self::$total);


            // debug(array_sum(self::$total));
            // debug($data ,false);
            // debug($commission ,false);
            // debug($the_rest_commission);

            $record = [
                    'user_id' => $user->ID,
                    'user_ref' => $user->user_login,
                    'user_login' => $user->user_login,
                    'order_id' => $order_id,
                    'order_status' => $order->get_status(),
                    'ref_path' => $ref_path,
                    'ref_product' => $ref_product ? $ref_product : NULL,
                    'ref_coupon' => isset($coupon) ? $ref_coupon . '-' . $coupon['value'] : NULL,
                    'status' => 0,
                    'date' => date('Y-m-d H:i:s'),
                    'total' => $order->get_total(),
                    'commission' => $commission,
                    'customer_name' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
                    'customer_phone' => $order->get_billing_phone(),
                    'description' => '+' . number_format($commission) . ' Hoa hồng của ID đơn hàng ' . $order_id . ' cho cộng tác viên ' . $user->user_login,
                    'order_json' => json_encode($order_json)
                    
                ];
            
            MH_Query::init(null, self::$table)->insert($record);
            self::setCommissionAncestors($user, $order, $commission, $order_json, $the_rest_commission);
        }
        

        static function setCommissionProductMode($user, $order, $ref_coupon){
            $settings = self::getSettings();
            $commission = 0;
            $commission_level = 0;

            $commission_percent_default = $settings['commission_percent_default'];
            $user_level = $settings['user_level'];
            if($user_level > 0)
            {
                $commission_user_levels = $settings['commission_user_levels'];
                $commission_level = $commission_user_levels[$user->level]['commission'] > 0 ? $commission_user_levels[$user->level]['commission']: 0;
                
            }

            // debug($user);
            // Apply Coupon
            if($ref_coupon)
                $coupon = MH_Query::init(null, 'mh_coupons')->where('coupon', $ref_coupon)->first();
            

            foreach ( $order->get_items() as  $item_key => $item_values ) {
                    $item_data = $item_values->get_data();
                    $line_total = $item_data['total'];

                    //Get Commission Setting By Product ID
                    $commission_setting = AFF_Commission_Settings::getCommissionSettingById($item_data['product_id']);
                    
                    if($commission_setting)
                    $commission_percent = $commission_setting + $commission_level;
                    else
                    $commission_percent = $commission_percent_default + $commission_level;

                    if(isset($coupon)){
                        $line_total = $line_total*100 / (100 - $coupon['value']);
                        $commission_percent = $commission_percent - $coupon['value'];
                        $commission_percent = $commission_percent < 0 ?  0  : $commission_percent;
                    }

                    $commission += ceil(($line_total/100) * $commission_percent);

            }
            
            return [
                'commission' => $commission,
                'commission_percent' => $commission_percent,
            ];

        }

        static function setCommissionOrderMode($user, $order, $ref_coupon){
            $settings = self::getSettings();
            if($user->commission_percent)
                $commission_percent = $user->commission_percent; // If User have certain commission percent then Omit all.
            else{
                $commission_percent_default = $settings['commission_percent_default'];
                $commission_level = 0;

                //User Level
                $user_level = $settings['user_level'];
                if($user_level > 0)
                {
                    $commission_user_levels = $settings['commission_user_levels'];
                    $commission_level = $commission_user_levels[$user->level]['commission'] > 0 ? $commission_user_levels[$user->level]['commission']: 0;

                    
                }
            }
            // Order Mode - Include Shipping Fee And Tax
            $total = $order->get_total();
            if($settings['aff_commission_include_order_shipping'] == 'false')
                $total = $order->get_total() - $order->get_total_tax() - $order->get_total_shipping() - $order->get_shipping_tax();

                
            //Final Commission    
            $commission_percent = $commission_percent_default + $commission_level;

            //Apply Coupon
            if($ref_coupon){
                    $coupon = MH_Query::init(null, 'mh_coupons')->where('coupon', $ref_coupon)->first();
                    if($coupon){
                        $total = $total*100 / (100 - $coupon['value']);
                        $commission_percent = $commission_percent - $coupon['value'];
                        $commission_percent = $commission_percent < 0 ?  0  : $commission_percent;

                    }
            }

            
            $commission = ceil(($total/100) * $commission_percent);

            return [
                'commission' => $commission,
                'commission_percent' => $commission_percent,
            ];

        }

     

        static function setCommissionAncestors($user, $order, $commission, $order_json, $the_rest_commission){
            $settings = self::getSettings();
            $commission_relationship_levels = $settings['commission_relationship_levels'];

            if($settings['relationship_level']){
                $ancestors = AFF_User_Relationship::getAncestor($user->ID, $settings['relationship_level']);
                if($ancestors){
                  
                    $order_id = $order->get_id();
                    $order_total = $order->get_total();
                    $cutomer_name = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
                    $customer_phone = $order->get_billing_phone();


                    $the_rest_percent = 100;

                    foreach ($ancestors as $key => &$an) {
                        if($an['distance'] == 0)
                            continue;
                        $commission_percent = floatval($commission_relationship_levels[$an['distance'] - 1]['commission']);
                        $an['commission_percent'] = $commission_percent;
                        $the_rest_percent -= $commission_percent;
                    }
                    if(isset($ancestors[1]))
                        $ancestors[1]['commission_percent'] += $the_rest_percent;
                    // debug($ancestors);


                    foreach ($ancestors as $key => &$an) {
                        if($an['distance'] == 0)
                            continue;
                            // debug($an, false);
                            // $commission_percent = floatval($commission_relationship_levels[$an['distance'] - 1]['commission']);


                            //Mode
                            // if($settings['commission_relationship_mode'] == 'commission')
                            //     $commission_level = $commission/100 * $commission_percent;
                            // else
                            //     $commission_level = $order_total/100 * $commission_percent;

                            $commission_level = $the_rest_commission/100 * $an['commission_percent'];



                            $parent_user = get_user_by('id', $an['ancestor_id']);
                            if($parent_user){
                                $data = [
                                    'user_id' => $an['ancestor_id'],
                                    'user_ref' => $user->user_login,
                                    'user_login' => $parent_user->user_login,
                                    'order_id' => $order->get_id(),
                                    'order_status' => $order->get_status(),
                                    'ref_path' => NULL,
                                    'ref_product' => NULL,
                                    'ref_coupon' => NULL,
                                    'status' => 0,
                                    'date' => date('Y-m-d H:i:s'),
                                    'total' => $order_total,
                                    'level' => $an['distance'],
                                    'commission' => $commission_level,
                                    'customer_name' => $cutomer_name,
                                    'customer_phone' => $customer_phone,
                                    'order_json' => json_encode($order_json),
                                    'description' => '+' . number_format($commission_level) . ' Hoa hồng của ID đơn hàng ' . $order_id . ' cho tài khoản ' . $parent_user->user_login . ' từ cộng tác viên cấp ' . $an['distance'] . ' ' . $user->user_login

                                
                                ];
                                MH_Query::init(null, self::$table)->insert($data);
                            }
                            
                    }

                    die();
                }
            }
        }


        static function getOrderStats($filters){
            $data = [
                'orders' => 0,
                'completed_orders' => 0,
            ];

            $query = MH_Query::init(null, self::$table)->select('status, count(id) as total');
            $query = self::buildQuery($query, $filters);
            $result = $query->group_by('status')->get();
            if($result){
                foreach ($result as $r) {
                    if($r['status'] == 1)
                        $data['completed_orders'] = intval($r['total']);

                    $data['orders'] += $r['total'];

                }
            }
            return $data;
        }

        static function getIncomeStats($filters){
            $data = [
                'income' => 0,
                'approved_income' => 0,
            ];

            $query = MH_Query::init(null, self::$table)->select('status, sum(total) as total');
            $query = self::buildQuery($query, $filters);
            $result = $query->group_by('status')->get();
            if($result){
                foreach ($result as $r) {
                    if($r['status'] == 1)
                        $data['approved_income'] = intval($r['total']);

                        $data['income'] += $r['total'];

                }
            }
            return $data;
        }

        static function getCommissionStats($filters){
            $data = [
                'commission' => 0,
                'approved_commission' => 0,
            ];

            $query = MH_Query::init(null, self::$table)->select('status, sum(commission) as total');
            $query = self::buildQuery($query, $filters);
            $result = $query->group_by('status')->get();
            if($result){
                foreach ($result as $r) {
                    if($r['status'] == 1)
                        $data['approved_commission'] = intval($r['total']);

                        $data['commission'] += $r['total'];

                }
            }
            return $data;
        }

        static function getUserOrder($filters){
            $query = MH_Query::init(null, self::$table);
            $query = self::buildQuery($query, $filters);
            return $query->order_by('id', 'DESC')->get();
        }

        static function getUserOrderHistory($filters, $page = 1, $per_page = 15){
            $query = MH_Query::init(null, self::$table);
            $query = self::buildQuery($query, $filters)->order_by('id', 'DESC');
            $data = $query->page($page, $per_page)->get(ARRAY_A, '', true);
            return ['data' => $data ? $data : [], 'pagination' => $pagination = $query->rows_found($page, $per_page)];
        }



        static function getOrderStatByDay($filters){
            $date_range = MH_Date_Array($filters['date_range'][0], $filters['date_range'][1]);

            
            $query = MH_Query::init(null, self::$table)->select('DATE_FORMAT(date, "%d-%m-%Y")  as date, count(id) as total')->where('level', 0);
            $query = self::buildQuery($query, $filters);
            $result_orders = $query->group_by('DATE_FORMAT(date, "%d-%m-%Y")')->order_by('date')->get();
            
            $query = MH_Query::init(null, self::$table)->select('DATE_FORMAT(date, "%d-%m-%Y")  as date, count(id) as total')->where('status', 1)->where('level', 0);
            $query = self::buildQuery($query, $filters);
            $result_completed_orders = $query->group_by('DATE_FORMAT(date, "%d-%m-%Y")')->order_by('date')->get();

            $query = MH_Query::init(null, 'mh_traffics')->select('DATE_FORMAT(date, "%d-%m-%Y") as date,sum(total) as total');
            unset($filters['level']);
            $query = self::buildQuery($query, $filters);
            
            $result_views = $query->group_by('date')->order_by('date')->get();

            $chart_adapter = [
                'orders' => &$result_orders,
                'completed_orders' => &$result_completed_orders,
                'views' => &$result_views
            ];
            $chart_stats = [];
            $data = [];
            
            foreach ($date_range as $k1 => $date) {
                foreach ($chart_adapter as $k2 => &$c) {
                        $chart_stats[$k2][$date] = 0;
                }
            }
            foreach ($chart_adapter as $k2 => &$c) {
                foreach($c as $k3 => $t) {
                    $formatDate = explode('-', $t['date']);
                    $formatDate = $formatDate[0] . '-' . $formatDate[1];
                    $chart_stats[$k2][$formatDate] = intval($t['total']);
                }
            }
            // return $chart_stats;

            
            foreach ($chart_stats as $k1 => &$c) {
                foreach ($c as $key => $t) {
                    $data[$k1][] = $t;
                }
            }


            $data['date_range'] = $date_range;
            return $data;


        
          
        }


        static function updateOrderStatus($order_id, $status){
            MH_Query::init(null, self::$table)->where('order_id', $order_id)->update(['order_status' => $status]);
        }

        static function  buildQuery($query, $filters){
            if(isset($filters['status'])){
                $query = $query->where('status', $filters['status']);
            }
            if(isset($filters['user_id']) && $filters['user_id']){
                $query = $query->where('user_id', $filters['user_id']);
            }
            if(isset($filters['level'])){
                if(is_array($filters['level']))
                    $query = $query->where('level', $filters['level'][0], $filters['level'][1]);
                else
                    $query = $query->where('level', $filters['level']);
            }
            if(isset($filters['date_range']) && $filters['date_range'][0]){
                $date_range = array_unique( $filters['date_range'] );
                if(sizeof($date_range) == 1)
                    $query = $query->where('DATE(date)', $date_range[0]);
                else
                    $query = $query->whereRaw("(date BETWEEN '$date_range[0]' AND '$date_range[1] 23:59:59')");
            }

            if(isset($filters['search']) && $filters['search']){
                $query = $query->search($filters['search'], ['order_id', 'user_login'], 'OR');
            }

            return $query;
        }

        

        static function moveOrderToTrash($order_id){
            $order = MH_Query::init(null, self::$table)->where('order_id', $order_id)->where('order_status', '!=' , 'completed')->first();
            if($order)
                MH_Query::init(null, self::$table)->where('order_id', $order_id)->update(['order_status' => 'cancelled']);

        }

	    


	}
    // AFF_User_Order::moveOrderToTrash(506);

?>
