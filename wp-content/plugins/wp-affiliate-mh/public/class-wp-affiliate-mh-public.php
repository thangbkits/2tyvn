<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://dominhhai.com
 * @since      1.0.0
 *
 * @package    Wp_Affiliate_Mh
 * @subpackage Wp_Affiliate_Mh/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wp_Affiliate_Mh
 * @subpackage Wp_Affiliate_Mh/public
 * @author     Đỗ Minh Hải <minhhai27121994@gmail.com>
 */
class Wp_Affiliate_Mh_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	private $commission_percent_default;

	public $user = null;
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;	
		$this->commission_percent_default = AFF_Config::getConfig('commission_percent_default');
		$this->commission_user_levels = AFF_Config::getConfig('commission_user_levels');
		$this->user_level = AFF_Config::getConfig('user_level');
		//Add Billing Fields
		add_action( 'woocommerce_checkout_billing', [$this, 'woocommerce_checkout_billing'] );
		add_action( 'woocommerce_checkout_create_order', [$this, 'woocommerce_checkout_create_order'], 20, 1 );

		//Handle commission 
		add_action( 'woocommerce_checkout_order_processed', [$this, 'woocommerce_checkout_order_processed'], 10, 3 );

		//Hanle when order is complete
		add_action( 'woocommerce_order_status_changed', [$this, 'woocommerce_order_status_changed'], 10, 4 );

		add_action('wp_loaded', [$this, 'wp_loaded']);

		//Auto active Affiliate
		add_action( 'user_register', [$this, 'user_register'] );

		//Add Configs to FE
		add_action('wp_footer', [$this, 'wp_footer']); 

		//Addshortcode UserDashboard
		add_shortcode('aff_user_dashboard', [$this, 'aff_user_dashboard']);

		add_action('woocommerce_after_shop_loop_item_title', [$this, 'woocommerce_after_shop_loop_item_title']);

		

	}

	public function wp_loaded(){
		if(is_user_logged_in()){
            $this->user = wp_get_current_user();
            add_action("woocommerce_before_shop_loop_item_title", [$this, "copy_affiliate_link"]);
        }
		// debug(AFF_User::getUserTree2(['user_id' => 1]));
		$this->user = wp_get_current_user();
		// Set view
		if(isset($_GET['ref'])){
			// if(((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443))
				$this->view(sanitize_text_field($_GET['ref']));
		}
	}

	public function woocommerce_after_shop_loop_item_title(){
		if(!$this->user)
			return;
		$product_id = get_the_ID();
		$db = MH_Query::init(null, 'mh_commission_settings');
		$db->where('type', 'product');
		$db->where('object_id', $product_id);
		$record = $db->first();
		$commission = $this->commission_percent_default;
		if($record)
			$commission = $record['commission'];
		if($this->user_level > 0){
			$commission_user_levels = json_decode($this->commission_user_levels, true);
			$commission_level = $commission_user_levels[$this->user->level]['commission'] > 0 ? $commission_user_levels[$this->user->level]['commission']: 0;
			$commission += $commission_level;
		}
		
		?>
			<div><span class="commisssion-bagde">CK: <?php echo $commission ?> %</span></div> 
		<?php
	}

	public function copy_affiliate_link(){
			echo '<a class="aff-copy-link" href="'.get_the_permalink().'?ref='.$this->user->user_login.'"><img class="aff-image-copy-link" src="'.AFF_URL.'/public/images/link-closed-flat.png" alt="Link free icon" title="Link giới thiệu cho người mua hàng"></a>'; 
	}

	public function view($login){
		$user = get_user_by( 'login', $login );
		$date = date('Y-m-d');
        if($user)
        {
          
          $url = aff_reconstruct_url(false);
          AFF_Traffic::setView($user->ID, $url, $date);
        }
	}

	public function aff_user_dashboard(){
		return MH_Load_view('wp-affiliate-mh-public-display.php');
	}

	public function user_register($user_id){

		$check_user_relationship = MH_Query::init(null, 'mh_user_relationships')->where('ancestor_id', $user_id)->where('descendant_id', $user_id)->first();
		if(!$check_user_relationship){
			MH_Query::init(null, 'mh_user_relationships')->insert([
				'ancestor_id' => $user_id,
				'descendant_id' => $user_id,
				'distance' => 0,
			]);
		}
		
		
         $aff_auto_active = AFF_Config::getConfig('aff_auto_active');
         if($aff_auto_active == 'true'){
           AFF_User::update(['ID' => $user_id, 'aff_active' => 1]);
         }
    }

	public function woocommerce_order_status_changed($id, $status_transition_from, $status_transition_to, $that){
		if($status_transition_to === 'completed')
			AFF_User_Order::approveCommission($id, $status_transition_to);
		
		AFF_User_Order::updateOrderStatus($id, $status_transition_to);
	}


	public function woocommerce_checkout_order_processed($order_id, $posted_data, $order){
		$ref_id = get_post_meta($order_id, '_ref_id', true);
        $ref_path = get_post_meta($order_id, '_ref_path', true);
        $ref_coupon = get_post_meta($order_id, '_ref_coupon', true);
        $ref_product = get_post_meta($order_id, '_ref_product', true);

		// $user = get_user_by('login', 'haihai');
		$user = get_user_by('login', $ref_id);
		
        $allow_order_self = AFF_Config::getConfig('allow_order_self');

		if($allow_order_self == 'true'){
			if(!$user){
				$ref_path = get_site_url();
				$user = wp_get_current_user();
			}
		}

		if($user && $user->aff_active)
        {
			

			AFF_User_Order::create($user, $order, $ref_path, $ref_product, $ref_coupon);
        }

	}
	

	public function woocommerce_checkout_create_order($order){

        if (isset($_POST['ref_id'])) {
            $ref_id = $_POST['ref_id'];
            if (!empty($ref_id)){
              $order->update_meta_data('_ref_id', $ref_id);
            } 
        }

        if (isset($_POST['ref_path'])) {
            $ref_path = $_POST['ref_path'];
            if (!empty($ref_path)){
              $order->update_meta_data('_ref_path', $ref_path);
            } 
        }

        if (isset($_POST['ref_coupon'])) {
            $ref_coupon = $_POST['ref_coupon'];
            if (!empty($ref_coupon)){
              $order->update_meta_data('_ref_coupon', $ref_coupon);
            } 
        }

        if (isset($_POST['ref_product'])) {
            $ref_product = $_POST['ref_product'];
            if (!empty($ref_product)){
              $order->update_meta_data('_ref_product', $ref_product);
            } 
        }
    
	}

	public function woocommerce_checkout_billing(){
		 
        woocommerce_form_field( 'ref_id', array(
            'type'          => 'hidden',
            'class'         => array( 'ref_id' ),
            'label'         => __( '' ),
            'placeholder'   => __( '' ),
          ), '');

        woocommerce_form_field( 'ref_path', array(
            'type'          => 'hidden',
            'class'         => array( 'ref_path' ),
            'label'         => __( '' ),
            'placeholder'   => __( '' ),
          ), '');


        woocommerce_form_field( 'ref_product', array(
            'type'          => 'hidden',
            'class'         => array( 'ref_product' ),
            'label'         => __( '' ),
            'placeholder'   => __( '' ),
          ), '');

        woocommerce_form_field( 'ref_coupon', array(
            'type'          => 'hidden',
            'class'         => array( 'ref_coupon' ),
            'label'         => __( '' ),
            'placeholder'   => __( '' ),
          ), '' );
       
	}


	public function wp_footer(){
		
		$time = AFF_Config::getConfig('aff_cookie_time');
      	$time = $time ? $time : 1;
      	$once = AFF_Config::getConfig('aff_cookie_once');
      	$cookie_traffic_mode = AFF_Config::getConfig('cookie_traffic_mode');
		
		$settings = [
			'time' => $time,
			'once' => $once,
			'cookie_traffic_mode' => $cookie_traffic_mode,
			'current_user' => $this->user->user_login,
			'ajax_url'		=> admin_url('admin-ajax.php')
		];

		echo "<input type='hidden' id='aff_settings' data-settings='".json_encode($settings)."'>";
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Affiliate_Mh_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Affiliate_Mh_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-affiliate-mh-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Affiliate_Mh_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Affiliate_Mh_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name . '-sweet-alert', AFF_URL . '/admin/js/sweetalert2@10.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-affiliate-mh-public.js', array( 'jquery' ), $this->version, false );

	}

}
