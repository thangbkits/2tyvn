

const template = `
<div class="q-pa-md" v-if="!isLoading">
    <div class="row">
        <div class="col-md-6 col-xs-12 text-center card-item">
            <transition appear enter-active-class="animated zoomIn" :duration="1000">
                <div class="flex justify-center">
                    <!-- <img src="~assets/vbimex.jpg" width="150px" style="position:relative; left:-15px"> -->
                </div>
            </transition>
            <p class="text-h6 q-mb-md">THÔNG TIN TÀI KHOẢN</p>
            <q-form @submit="onSubmit" class="q-gutter-sm">

                <q-chip :color="LEVEL_COLOR[user.level]" icon="stars" text-color="white">{{settings.commission_user_levels?.[user.level].name}}</q-chip>
                <q-chip color="orange" text-color="white" icon="attach_money">Số dư: {{addCommas(user.balance)}} đ</q-chip>
                <q-chip color="purple" text-color="white" icon="shopping_cart">Doanh số: {{addCommas(user.income)}} đ</q-chip>
                <q-input disable filled type="text" v-model="user.user_login" label="Tên đăng nhập *" lazy-rules
                    :rules="[val => (val && val.length > 0) || 'Điền tên tài khoản']" />
        
                <q-input disable filled type="text" v-model="user.display_name" label="Tên hiển thị" lazy-rules
                    :rules="[val => (val && val.length > 0) || 'Điền tên hiển thị']" />

                
                <q-input disable filled type="text" v-model="user.user_phone" label="Số điện thoại *" lazy-rules />
        
                <q-input disable filled type="email" v-model="user.user_email" label="Email *" lazy-rules :rules="[
                                  val => (val && val.length > 0) || 'Điền tên email',
                                  val =>
                                    (val && validateEmail(val)) || 'Định dạng Email không chính xác'
                                ]" />
        
                <q-input filled type="password" v-model="user.password" label="Đổi mật khẩu mới *" lazy-rules :rules="[
                              val => (val && val.length > 0) || 'Vui lòng điền mật khẩu'
                            ]" />
        
        
                <q-input filled type="password" v-model="user.password_confirmation" label="Nhập lại mật khẩu *" lazy-rules
                    :rules="[
                                  val => (val !== null && val !== '') || 'Nhập lại mật khẩu',
                                  val =>
                                    (val && val == user.password) || 'Mật khẩu bạn nhập không khớp'
                                ]" />
        
        
                <div>
                    <q-btn label="Cập nhật" type="submit" color="primary" />
                    <!-- <div class="q-mt-md">
                      Bạn chưa có tài khoản,
                      <router-link to="/user/register">đăng kí tại đây</router-link>
                    </div> -->
                </div>
            </q-form>
        </div>
    </div>
</div>
`;
import { updateUserProfile, getUserProfile } from '../api/user.js'
import {  LEVEL_COLOR } from "../constants/constants.js"
const { RV_CONFIGS } = window 
export default {
    data: () => ({
        configs: RV_CONFIGS,
        isLoading: true,
        settings: {},
        user: {
            user_login: "",
            display_name: "",
            user_phone: "",
            user_email: "",
            password: "",
            password_confirmation: "",
            ref: '',
            balance: 0,
            income: 0,
        },
        LEVEL_COLOR

    }),
   
    methods: {
        onSubmit() {
            this.$q.loading.show()
            updateUserProfile(this.user).then(res => {
                const { success, msg } = res.data
                this.NOTIFY(msg, success)
                this.$q.loading.hide()
            })
        },
        async getData(){
            this.$q.loading.show()
            const res = await getUserProfile(this.configs.user_id)
            const {data} = res.data
            this.user = data
            this.$q.loading.hide()
            this.isLoading = false
        }
	},
	components:{
       
	},
    watch:{

    },
    template: template,
    created(){
        this.getConfigs().then(res => {
            this.settings = res
        })
        this.getData();
        this.$eventBus.$emit('set.page_title', 'Thông tin tài khoản');
    }

}