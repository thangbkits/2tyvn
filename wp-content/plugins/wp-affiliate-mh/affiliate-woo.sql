-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th10 20, 2021 lúc 09:10 AM
-- Phiên bản máy phục vụ: 8.0.18
-- Phiên bản PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `affiliate-woo`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_commission_settings`
--

CREATE TABLE `aw_mh_commission_settings` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `object_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_configs`
--

CREATE TABLE `aw_mh_configs` (
  `id` int(11) NOT NULL,
  `config_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `config_value` text NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `field_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `field_attributes` int(11) DEFAULT NULL,
  `autoload` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `aw_mh_configs`
--

INSERT INTO `aw_mh_configs` (`id`, `config_name`, `config_value`, `description`, `field_name`, `field_attributes`, `autoload`) VALUES
(10, 'warning_days_duplicate_order', '10', NULL, NULL, NULL, 0),
(11, 'commission_user_levels', '[{\"name\":\"C\\u1ea5p \\u0110\\u1ed3ng\",\"income\":\"0\",\"commission\":\"0\"},{\"name\":\"C\\u1ea5p B\\u1ea1c\",\"income\":\"2000000\",\"commission\":\"1\"},{\"name\":\"C\\u1ea5p V\\u00e0ng\",\"income\":\"3000000\",\"commission\":\"2\"},{\"name\":\"C\\u1ea5p Platinum\",\"income\":\"4000000\",\"commission\":\"3\"},{\"name\":\"Kim c\\u01b0\\u01a1ng\",\"income\":\"5000000\",\"commission\":\"5\"},{\"name\":\"\",\"income\":\"\",\"commission\":\"\"},{\"name\":\"\",\"income\":\"\",\"commission\":\"\"}]', NULL, NULL, NULL, 0),
(12, 'user_level', '5', NULL, NULL, NULL, 0),
(13, 'commission_relationship_levels', '[{\"name\":\"C\\u1ea5p 1\",\"income\":\"1\",\"commission\":\"3\"},{\"name\":\"C\\u1ea5p 2\",\"income\":\"2000000\",\"commission\":\"2\"},{\"name\":\"C\\u1ea5p 3\",\"income\":\"3000000\",\"commission\":\"1\"},{\"name\":\"C\\u1ea5p 4\",\"income\":\"4000000\",\"commission\":\"3\"},{\"name\":\"C\\u1ea5p 5\",\"income\":\"5000000\",\"commission\":\"5\"},{\"name\":\"C\\u1ea5p 6\",\"income\":\"\",\"commission\":\"\"},{\"name\":\"C\\u1ea5p 7\",\"income\":\"\",\"commission\":\"\"}]', NULL, NULL, NULL, 0),
(14, 'relationship_level', '2', NULL, NULL, NULL, 0),
(15, 'aff_mode', 'order_mode', NULL, NULL, NULL, 0),
(16, 'commission_percent_default', '11', NULL, NULL, NULL, 0),
(17, 'aff_commission_include_order_shipping', 'true', NULL, NULL, NULL, 0),
(18, 'aff_cookie_once', 'false', NULL, NULL, NULL, 0),
(19, 'aff_cookie_time', '21', NULL, NULL, NULL, 0),
(20, 'aff_min_request', '100001', NULL, NULL, NULL, 0),
(21, 'aff_auto_active', 'true', NULL, NULL, NULL, 0),
(23, 'auto_active_aff', 'false', NULL, NULL, NULL, 0),
(24, 'noti_general', '<b>123123123123</b><div><b>Vui long <a href=\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\">truy cập</a></b></div>', NULL, NULL, NULL, 0),
(25, 'noti_email_user_actived', 'Chúc mừng tài khoản [user_name] đã được kích hoạt tính năng Cộng tác viên trên website https://dominhhai.com', NULL, NULL, NULL, 0),
(26, 'noti_not_active', '<div><br></div>Tài khoản của bạn chưa được kích hoạt tính năng Affiliate, vui lòng liên hệ Admin để được kích hoạt để có thể trở thành cộng tác viên.<div><br></div>', NULL, NULL, NULL, 0),
(27, 'noti_email_creat_payment_request', 'Thông báo bạn vừa tạo yêu cầu rút tiền cho tài khoản [user_name] trên hệ thống https://dominhhai.com\n- Số tiền: [total]\n- Thông tin ngân hàng: [bank_infomation]\nNếu có thắc mắc xin vui lòng liên hệ, chúng tôi sẽ duyệt lệnh thanh toán của bạn trong thời gian sớm nhất.', NULL, NULL, NULL, 0),
(28, 'noti_email_payment_request_completed', 'Chúc mừng bạn vừa được thanh toán thành công cho lệnh rút tiền tài khoản [user_name] trên hệ thống https://dominhhai.com\n- Số tiền: [total]\n- Thông tin ngân hàng: [bank_infomation]\nHãy kiểm tra lại tài khoản ngân hàng, Nếu có thắc mắc xin vui lòng liên hệ chúng tôi.', NULL, NULL, NULL, 0),
(29, 'noti_email_order_completed', 'Bạn vừa được duyệt hoa hồng trên hệ thống https://dominhhai.com\nMã đơn hàng:   #[order_id]\nTổng giá trị đơn hàng: [order_total] đ\nHoa hồng mà bạn nhận được: [commision]', NULL, NULL, NULL, 0),
(30, 'commission_relationship_mode', 'commission', NULL, NULL, NULL, 0),
(31, 'aff_email', 'true', NULL, NULL, NULL, 0),
(32, 'cookie_traffic_mode', 'false', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_coupons`
--

CREATE TABLE `aw_mh_coupons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon` varchar(255) NOT NULL,
  `value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_history`
--

CREATE TABLE `aw_mh_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_login` varchar(30) DEFAULT NULL,
  `amount` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `begin_balance` int(11) NOT NULL,
  `end_balance` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `aw_mh_history`
--

INSERT INTO `aw_mh_history` (`id`, `user_id`, `user_login`, `amount`, `description`, `begin_balance`, `end_balance`, `date`, `type`) VALUES
(152, 1, 'admin', '19500', '+19,500 Hoa hồng của ID đơn hàng 506 cho cộng tác viên admin', 3699899, 3719399, '2021-11-20 03:01:20', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_payments`
--

CREATE TABLE `aw_mh_payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_login` varchar(30) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `bank_info` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_traffics`
--

CREATE TABLE `aw_mh_traffics` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `product` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `aw_mh_traffics`
--

INSERT INTO `aw_mh_traffics` (`id`, `user_id`, `url`, `product`, `total`, `date`) VALUES
(76, 1, 'http://localhost/affiliate-woo/san-pham/beanie/', 54, 1, '2021-11-18'),
(77, 1, 'http://localhost/affiliate-woo/san-pham/beanie-with-logo/', 71, 6, '2021-11-18'),
(78, 1, 'http://localhost/affiliate-woo/san-pham/beanie/', 54, 2, '2021-11-20'),
(79, 7, 'http://localhost/affiliate-woo/san-pham/beanie/', 54, 1, '2021-11-20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_user_order`
--

CREATE TABLE `aw_mh_user_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_login` varchar(30) DEFAULT NULL,
  `user_ref` varchar(30) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `order_status` varchar(20) DEFAULT NULL,
  `ref_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ref_product` int(11) DEFAULT NULL,
  `ref_coupon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `customer_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `customer_phone` varchar(15) DEFAULT NULL,
  `date` datetime NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `total` int(11) NOT NULL,
  `commission` int(11) DEFAULT NULL,
  `is_paid` int(255) DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `order_json` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `aw_mh_user_order`
--

INSERT INTO `aw_mh_user_order` (`id`, `user_id`, `user_login`, `user_ref`, `order_id`, `order_status`, `ref_path`, `ref_product`, `ref_coupon`, `customer_name`, `customer_phone`, `date`, `status`, `total`, `commission`, `is_paid`, `level`, `description`, `payment_date`, `order_json`) VALUES
(176, 1, 'admin', 'admin', 506, 'completed', 'http://localhost/affiliate-woo/san-pham/beanie/', 54, NULL, 'do minh', '0336564989', '2021-11-20 02:58:51', '1', 150000, 19500, 0, 0, '+19,500 Hoa hồng của ID đơn hàng 506 cho cộng tác viên admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `aw_mh_user_relationships`
--

CREATE TABLE `aw_mh_user_relationships` (
  `id` int(11) NOT NULL,
  `ancestor_id` int(11) NOT NULL,
  `descendant_id` int(11) NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `aw_mh_user_relationships`
--

INSERT INTO `aw_mh_user_relationships` (`id`, `ancestor_id`, `descendant_id`, `distance`) VALUES
(12, 5, 5, 0),
(13, 1, 1, 0),
(14, 2, 2, 0),
(15, 3, 3, 0),
(16, 4, 4, 0),
(17, 1, 5, 1),
(18, 6, 6, 0),
(19, 1, 6, 1),
(20, 7, 7, 0),
(21, 6, 7, 1),
(22, 1, 7, 2),
(23, 8, 8, 0),
(24, 7, 8, 1),
(25, 6, 8, 2),
(26, 9, 9, 0),
(27, 1, 9, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `aw_mh_commission_settings`
--
ALTER TABLE `aw_mh_commission_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_configs`
--
ALTER TABLE `aw_mh_configs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_coupons`
--
ALTER TABLE `aw_mh_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_history`
--
ALTER TABLE `aw_mh_history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_payments`
--
ALTER TABLE `aw_mh_payments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_traffics`
--
ALTER TABLE `aw_mh_traffics`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_user_order`
--
ALTER TABLE `aw_mh_user_order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `aw_mh_user_relationships`
--
ALTER TABLE `aw_mh_user_relationships`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `aw_mh_commission_settings`
--
ALTER TABLE `aw_mh_commission_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT cho bảng `aw_mh_configs`
--
ALTER TABLE `aw_mh_configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `aw_mh_coupons`
--
ALTER TABLE `aw_mh_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `aw_mh_history`
--
ALTER TABLE `aw_mh_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT cho bảng `aw_mh_payments`
--
ALTER TABLE `aw_mh_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `aw_mh_traffics`
--
ALTER TABLE `aw_mh_traffics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT cho bảng `aw_mh_user_order`
--
ALTER TABLE `aw_mh_user_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT cho bảng `aw_mh_user_relationships`
--
ALTER TABLE `aw_mh_user_relationships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
